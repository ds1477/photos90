package app;

import java.io.IOException;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Terminal;
import model.User;
import view.LoginController;
//import view.SceneController;

/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * This Photos class contains the main method and will run the application. 
 */

public class Photos extends Application{
	
	/**
	 * The primary stage
	 */
	Stage mainStage;

	@Override
	public void start(Stage primaryStage) throws Exception {

		mainStage = primaryStage;
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		AnchorPane root = (AnchorPane)loader.load(); 
		
		Terminal terminal = Terminal.readApp();
		if (terminal == null) {
			terminal = new Terminal();
			terminal.addUser("stock");
			User stock = terminal.getUser("stock");
			stock.addAlbum("Stock");
			Album stockalbum = stock.getAlbum("Stock");
			stockalbum.addStockPhoto(new Photo("data/cat1.jpeg"));
			stockalbum.addStockPhoto(new Photo("data/cat2.jpg"));
			stockalbum.addStockPhoto(new Photo("data/cat3.jpg"));
			stockalbum.addStockPhoto(new Photo("data/cat4.jpg"));
			stockalbum.addStockPhoto(new Photo("data/cat5.jpg"));
			stockalbum.addStockPhoto(new Photo("data/cat6.jpg"));
		}
		
		LoginController controller = loader.getController(); //we get a controller instance
		controller.start(primaryStage, terminal); //that we then call the start method on
		
		Scene scene = new Scene(root); //sending in width and height in pixels
		mainStage.setResizable(false);
		mainStage.setScene(scene); 
		mainStage.sizeToScene();
		mainStage.show();
		
		
		final Terminal term = new Terminal(terminal);
		primaryStage.setOnCloseRequest(event -> { 
			try {
				Terminal.writeApp(term);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}