package view;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The MoveCopyController class is the controller for move_copy.fxml.
 */
public class MoveCopyController {
	
	/**
	 * Button to return to Photos page
	 */
	@FXML
	public Button back;
	
	/**
	 * RadioButton to represent move mode
	 */
	@FXML
	public RadioButton move;
	
	/**
	 * RadioButton to represent copy mode
	 */
	@FXML
	public RadioButton copy;
	
	/**
	 * Button that executes move or copy
	 */
	@FXML
	public Button commit;
	
	/**
	 * ListView to display all albums that the user has
	 */
	@FXML
	public ListView<String> listview;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * The name of the album that the activePhoto is in
	 */
	String thisalbum;
	
	/**
	 * The activePhoto
	 */
	Photo currentPhoto;
	
	/**
	 * The activeUser
	 */
	User currentUser;
	
	/**
	 * The activeAlbum
	 */
	Album currentAlbum;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		
		ToggleGroup toggle = new ToggleGroup();
		move.setToggleGroup(toggle);
		copy.setToggleGroup(toggle);
		
		currentUser = terminal.getActiveUser();
		ObservableList<String> albumNames = FXCollections.observableArrayList();
		ArrayList<Album> albums = currentUser.getAllAlbums();
		currentAlbum = terminal.getActiveAlbum();
		thisalbum = Album.getAlbumName(currentAlbum);
		currentPhoto = terminal.getActivePhoto();
		
		for (Album album : albums) {
			albumNames.add(Album.getAlbumName(album));
		}
		move.setSelected(true);
		listview.setItems(albumNames);
	}
	
	/**
	 * Called when 'commit' is clicked. Depending on if the move or copy radio buttons are selected, will attempt to execute action into selected album.
	 */
	public void commitChanges() {
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Action was unsuccessful");
			alert.setContentText("Please select an album!");
			alert.showAndWait();
		} else if (listview.getSelectionModel().getSelectedItem().equals(thisalbum)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Action was rejected.");
			alert.setContentText("Album already containing this photo was selected. Please select a different album.");
			alert.showAndWait();
		} else {
			String toalbum = listview.getSelectionModel().getSelectedItem();
			Album thatalbum = currentUser.getAlbum(toalbum);
			if (thatalbum.getPhotos().contains(currentPhoto)) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Action was rejected.");
				alert.setContentText("Album already containing this photo was selected. Please select a different album.");
				alert.showAndWait();
			} else {
				if (copy.isSelected()) {
					thatalbum.addStockPhoto(currentPhoto); //not stock, but this should be safe
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText("Copy complete.");
					alert.setContentText("Successfully copied photo into album.");
					alert.showAndWait();
				} else {
					thatalbum.addStockPhoto(currentPhoto);
					currentAlbum.deletePhoto(currentPhoto);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText("Move complete.");
					alert.setContentText("Successfully moved photo into album. Now returning to photos page.");
					alert.showAndWait();
					try {
						goBack();
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("Unexpected Error");
					}
				}
			}
		}
	}
	
	/**
	 * Called when 'back' is clicked. Returns to Photos page.
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/photos.fxml"));
		Parent pane = loader.load(); 
		terminal.closeActivePhoto();
		
		PhotosController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	
}