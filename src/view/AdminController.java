package view;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The AdminController class is the controller for admin.fxml.
 */
public class AdminController {
	
	/**
	 * ListView that displays the list of users
	 */
	@FXML
	public ListView<String> listview;
	
	/**
	 * TextField that accepts a new username
	 */
	@FXML
	public TextField newUser;
	
	/**
	 * Button to create a new user
	 */
	@FXML
	public Button create;
	
	/**
	 * Button to delete the selected user
	 */
	@FXML 
	public Button delete;
	
	/**
	 * Button to go back to login page
	 */
	@FXML
	public Button logout;
		
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		ObservableList<String> usernames = FXCollections.observableArrayList();
		ArrayList<String> names = terminal.getUsers();
		for (String name : names) {
			usernames.add(name);
		}
		listview.setItems(usernames);
	}
	
	/**
	 * Called when 'create' is clicked. Attempts to create a new user.
	 */
	public void createUser() {
			String username = newUser.getText().replaceAll("\\s","");
			if (username.trim().length() > 0) {
				if (terminal.addUser(username)) {
					listview.getItems().add(username); 
					newUser.clear();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Adding new user has been unsuccessful.");
					alert.setContentText("This user already exists!");
					newUser.clear();
					alert.showAndWait();
				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Adding new user has been unsuccessful.");
				alert.setContentText("You must enter a valid username!");
				newUser.clear();
				alert.showAndWait();
			}
			
	}
	
	/**
	 * Called when 'delete' is clicked. Attempts to delete the selected user.
	 */
	public void deleteUser() {
			if (listview.getSelectionModel().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Deleting user has been unsuccessful.");
				alert.setContentText("Please select a user to delete.");
				alert.showAndWait();
			} else if (listview.getSelectionModel().getSelectedItem().equals("stock")) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Deleting user has been unsuccessful.");
				alert.setContentText("Cannot delete the stock user.");
				alert.showAndWait();
			} else {
				String username = listview.getSelectionModel().getSelectedItem();
				if (terminal.deleteUser(username)) {
					listview.getItems().remove(listview.getSelectionModel().getSelectedIndex());
				}
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Deleting user has been unsuccessful.");
					alert.setContentText("Please try again.");
					alert.showAndWait();
				}
			}
	}
	
	/**
	 * Called when 'logout' is clicked. Returns to login page.
	 * @throws java.io.IOException
	 */
	public void logoutAttempt() throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent pane = loader.load(); 
		
		LoginController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
}