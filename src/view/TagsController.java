package view;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Photo;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The TagsController class is the controller for tags.fxml.
 */
public class TagsController {
	
	/**
	 * Button to return to Photos page
	 */
	@FXML
	public Button back;
	
	/**
	 * Button to add a tag,value pair to the photo
	 */
	@FXML
	public Button add;

	/**
	 * Button to remove the selected tag,value pair from a photo
	 */
	@FXML
	public Button remove;
	
	/**
	 * Button to create a new tag type
	 */
	@FXML
	public Button create;
	
	/**
	 * TextField for new tag type name
	 */
	@FXML
	public TextField newTag;
	
	/**
	 * TextField to display selected tag type
	 */
	@FXML
	public TextField selected;
	
	/**
	 * TextField for tag value
	 */
	@FXML
	public TextField value;
	
	/**
	 * ListView of tag types
	 */
	@FXML
	public ListView<String> typelist;
	
	/**
	 * ListView of tag,value pairs
	 */
	@FXML
	public ListView<String> taglist;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * The activePhoto
	 */
	Photo currentPhoto;
	
	/**
	 * The activeUser
	 */
	User currentUser;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		ObservableList<String> userTags = FXCollections.observableArrayList();
		ObservableList<String> photoTags = FXCollections.observableArrayList();
		
		selected.setEditable(false);
		currentPhoto = terminal.getActivePhoto();
		currentUser = terminal.getActiveUser();
		for (String tag : currentUser.getTagTypes()) {
			userTags.add(tag);
		}
		for (String tag : currentPhoto.getTags()) {
			photoTags.add(tag);
		}
		typelist.setItems(userTags);
		typelist.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> selected.setText(typelist.getSelectionModel().getSelectedItem()));
		taglist.setItems(photoTags);
		typelist.getSelectionModel().select(0);
	}
	
	/**
	 * Called when 'add' is clicked. Attempts to add a new tag,value pair to the photo
	 */
	public void addTag() {
		if (selected.getText() == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Creating photo tag has been unsuccessful.");
			alert.setContentText("Please select a tag name from the list of tag types.");
			alert.showAndWait();
		} else if (value.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Creating photo tag has been unsuccessful.");
			alert.setContentText("Please enter a tag value.");
			alert.showAndWait();
		} else if (value.getText().isBlank()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Creating photo tag has been unsuccessful.");
			alert.setContentText("Please enter a tag value.");
			alert.showAndWait();
		} else if (currentPhoto.addTag(selected.getText(), value.getText())) {
			String toAdd = "[" + selected.getText() + ": " + value.getText() + "]";
			taglist.getItems().add(toAdd);
			value.setText("");
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Creaing photo tag has been unsuccessful.");
			alert.setContentText("This tag already exists on this photo.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Called when 'remove' is clicked. Attempts to remove selected tag,value pair from photo
	 */
	public void removeTag() {
		if (taglist.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Deleting tag has been unsuccessful.");
			alert.setContentText("Please select a tag of the photo.");
			alert.showAndWait();
		} else {
			String remove = (String) taglist.getSelectionModel().getSelectedItem();
			currentPhoto.deleteTag(remove);
			taglist.getItems().remove(remove);
		}
	}
	
	/**
	 * Called when 'create' is clicked. Attempts to create a new tag type
	 */
	public void createTag() {
		String addthis = newTag.getText();
		if (newTag.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Adding tag name has been unsuccessful.");
			alert.setContentText("Please enter a tag name.");
			alert.showAndWait();
		} else if (newTag.getText().isBlank()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Adding tag name has been unsuccessful.");
			alert.setContentText("Please enter a tag name.");
			alert.showAndWait();
		} else if (currentUser.addTagType(addthis)) {
			typelist.getItems().add(addthis);
			newTag.setText("");
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Adding tag name has been unsuccessful.");
			alert.setContentText("This tag name already exists");
			alert.showAndWait();
		}
	}
	
	/**
	 * Called when 'back' is clicked. Returns to the Photos page.
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/photos.fxml"));
		Parent pane = loader.load(); 
		terminal.closeActivePhoto();
		
		PhotosController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	
}