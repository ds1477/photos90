package view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Photo;
import model.Terminal;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The CaptionController class is the controller for captions.fxml.
 */
public class CaptionController {
	
	/**
	 * Button to go back to Photos page
	 */
	@FXML
	public Button back;
	
	/**
	 * Button to save changes to caption and return to Photos
	 */
	@FXML
	public Button save;
	
	/**
	 * TextField that displays the caption, and allows edits
	 */
	@FXML
	public TextField caption;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		Photo currentPhoto = terminal.getActivePhoto();
		caption.setText(currentPhoto.getCaption());
		
	}
	
	/**
	 * Called when 'save' is clicked. Sets the caption to the contents of 'caption' TextField, then returns to Photos
	 */
	public void setCaption() {
		String newCaption = caption.getText();
		Photo currentPhoto = terminal.getActivePhoto();
		currentPhoto.setCaption(newCaption);
		try {
			goBack();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("An Unexpected Error Occurred");
		}
	}
	
	/**
	 * Called from setCaption or when 'back' is clicked. Returns to Photos
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/photos.fxml"));
		Parent pane = loader.load(); 
		
		PhotosController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	
}