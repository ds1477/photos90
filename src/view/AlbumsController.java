package view;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The AlbumsController class is the controller for albums.fxml.
 */
public class AlbumsController {
	
	/**
	 * Button to open the selected album, and go to its respective photos page
	 */
	@FXML
	public Button open;
	
	/**
	 * Log out button
	 */
	@FXML
	public Button logout;
	
	/**
	 * Button to go to the search page
	 */
	@FXML
	public Button search;
	
	/**
	 * ListView that lists all albums, how many photos are in each album, and their date ranges
	 */
	@FXML
	public ListView<String> listview;
	
	/**
	 * Button that creates an album
	 */
	@FXML
	public Button create;
	
	/**
	 * Button that changes the name of the selected album
	 */
	@FXML
	public Button change;
	
	/**
	 * Button that deletes the selected album
	 */
	@FXML 
	public Button delete;
	
	/**
	 * TextField for a new album name
	 */
	@FXML
	public TextField newAlbum;
	
	/**
	 * TextField for an album name to replace the selected album's name
	 */
	@FXML
	public TextField changedAlbum;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		
		ObservableList<String> albumlist = FXCollections.observableArrayList();
		ArrayList<Album> allAlbums = terminal.getActiveUser().getAllAlbums();
		ArrayList<Date> range;
		for (Album album : allAlbums) {
			String info = "Album Name: " + Album.getAlbumName(album) + ", Number of Photos: " + album.getNumberOfPhotos();
			range = album.getTimeRange();
			if (range.get(0) == null) {
				info = info + ", Earliest Date: N/A, Latest Date: N/A";
			} else {
				info = info + ", Earliest Date: " + range.get(0) + ", Latest Date: " + range.get(1);
			}
			albumlist.add(info);
		}
		listview.setItems(albumlist);
	}
	
	/**
	 * Called when 'open' is clicked. Attempts to go to the photos page for the selected album.
	 * @throws java.io.IOException
	 */
	public void openAlbum() throws IOException {
		String albuminfo = listview.getSelectionModel().getSelectedItem();
		if (albuminfo == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Unable to open album.");
			alert.setContentText("Please select an album to open.");
			alert.showAndWait();
		} else {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/photos.fxml"));
			Parent pane = loader.load(); 
			
			
			String albumname = albuminfo.split(", Number of Photos: ")[0];
			albumname = albumname.substring(12);
			terminal.setActiveAlbum(albumname);
			
			PhotosController controller = loader.getController(); 
			controller.start(this.primaryStage, this.terminal); 
			primaryStage.getScene().setRoot(pane);
			primaryStage.sizeToScene();
		}
	}
	
	/**
	 * Called when 'search' is clicked. Goes to the search page.
	 * @throws java.io.IOException
	 */
	public void toSearch() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/search.fxml"));
		Parent pane = loader.load(); 
		
		SearchController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	/**
	 * Called when 'create' is clicked. Attempts to create a new album.
	 */
	public void createAlbum() {
			User currentUser = terminal.getActiveUser();
			String albumName = newAlbum.getText().replaceAll("\\s","");
			if (albumName.trim().length() < 1) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Adding new album has been unsuccessful.");
				alert.setContentText("Please enter a valid album name!");
				newAlbum.clear();
				alert.showAndWait();
			} else {
				if (currentUser.addAlbum(albumName)) {
					String info = "Album Name: " + albumName + ", Number of Photos: 0, Earliest Date: N/A, Latest Date: N/A";
					listview.getItems().add(info); 
					newAlbum.clear();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Adding new album has been unsuccessful.");
					alert.setContentText("Something went wrong! Please try again.");
					newAlbum.clear();
					alert.showAndWait();
				}
			}
	}
	
	/**
	 * Called when 'change' is clicked. Attempts to update the selected albums name.
	 */
	public void changeAlbumName() {
			if (listview.getSelectionModel().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Changing album name has been unsuccessful.");
				alert.setContentText("Please select an album to alter.");
				alert.showAndWait();
			} else {
				if (changedAlbum.getText().isBlank()) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Changing album name has been unsuccessful.");
					alert.setContentText("Please enter a valid album name.");
					alert.showAndWait();
				} else {
					User currentUser = terminal.getActiveUser();
					String oldalbuminfo = listview.getSelectionModel().getSelectedItem();
					String oldAlbumName = oldalbuminfo.split(", Number of Photos: ")[0];
					String laterinfo = oldalbuminfo.split(", Number of Photos: ")[1];
					oldAlbumName = oldAlbumName.substring(12);
					int oldAlbumIndex = listview.getSelectionModel().getSelectedIndex();
					String newAlbumName = changedAlbum.getText();
					if (currentUser.changeAlbum(oldAlbumName, newAlbumName)) {
						listview.getItems().remove(oldAlbumIndex);
						String newinfo = "Album Name: " + newAlbumName + ", Number of Photos: " + laterinfo;
						listview.getItems().add(newinfo);
						changedAlbum.clear();
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error Dialog");
						alert.setHeaderText("Changing album name has been unsuccessful.");
						alert.setContentText("Please try again.");
						alert.showAndWait();
					}
				}
				
			}
	}
	
	/**
	 * Called when 'delete' is clicked. Attempts to delete selected album.
	 */
	public void deleteAlbum() {
			if (listview.getSelectionModel().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Deleting album has been unsuccessful.");
				alert.setContentText("Please select an album to delete.");
				alert.showAndWait();
			}
			else {
				User currentUser = terminal.getActiveUser();
				String albuminfo = listview.getSelectionModel().getSelectedItem();
				String albumName = albuminfo.split(", Number of Photos: ")[0];
				albumName = albumName.substring(12);
				if (currentUser.deleteAlbum(albumName)) {
					listview.getItems().remove(listview.getSelectionModel().getSelectedIndex());
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Deleting album has been unsuccessful.");
					alert.setContentText("Please try again.");
					alert.showAndWait();
				}
			}
	}
	
	/**
	 * Called when 'logout' is clicked. Returns to login page.
	 * @throws java.io.IOException
	 */
	public void logoutAttempt() throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent pane = loader.load(); 
		terminal.clearActives();
		
		LoginController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
}