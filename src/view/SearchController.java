package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The SearchController is the controller for search.fxml
 */
public class SearchController {
	
	/**
	 * ListView to show search results
	 */
	@FXML
	public ListView<String> listview; 
	
	/**
	 * The drop down menu for the first tag
	 */
	@FXML 
	public ChoiceBox<String> choiceBox1;

	/**
	 * The drop down menu for the second tag
	 */
	@FXML
	public ChoiceBox<String> choiceBox2;
	
	/**
	 * The text field for the first tag value
	 */
	@FXML
	public TextField firstTag;
	
	/**
	 * The text field for the second tag value
	 */
	@FXML
	public TextField secondTag;

	/**
	 * The starting date to search from
	 */
	@FXML
	public DatePicker fromDate;
	
	/**
	 * The ending date to search to
	 */
	@FXML
	public DatePicker toDate;
	
	/**
	 * Button to create album from search results
	 */
	@FXML
	public Button createAlbum;
	
	/**
	 * Button to search based either date or tags
	 */
	@FXML 
	public Button search;
	
	/**
	 * Radio button to select search by date
	 */
	@FXML
	public RadioButton searchByDate;
	
	/**
	 * Radio button to select to search by tags
	 */
	@FXML
	public RadioButton searchByTags;
	
	/**
	 * Radio button to enable searching via one tag
	 */
	@FXML
	public RadioButton searchByOneTag;
	
	/**
	 * Radio button to enable searching via two tags
	 */
	@FXML
	public RadioButton searchByTwoTags;
	
	/**
	 * Radio button to enable conjunctive searching via two tags
	 */
	@FXML
	public RadioButton andOperator;
	
	/**
	 * Radio button to enable disjunctive searching via two tags
	 */
	@FXML
	public RadioButton orOperator;
	
	/**
	 * Button to return to Albums page
	 */
	@FXML
	public Button back;
	
	/**
	 * The logout button
	 */
	@FXML
	public Button logout;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * ObservableList to store search result photo paths
	 */
	ObservableList<String> paths;
	
	/**
	 * ObservableList to store captions of search result photos
	 */
	ObservableList<String> captions;
	
	/**
	 * Album storing search results
	 */
	Album searchResultsAlbum;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		
		User currentUser = terminal.getActiveUser();
		ArrayList<String> tagTypes = currentUser.getTagTypes();
		
		for (String tag : tagTypes) {
			choiceBox1.getItems().add(tag);
			choiceBox2.getItems().add(tag);
		}
		
		ToggleGroup searchBy = new ToggleGroup();		
		searchByDate.setToggleGroup(searchBy);
		searchByTags.setToggleGroup(searchBy);
		
		// listen to changes in selected toggle
		searchBy.selectedToggleProperty().addListener((observable, oldVal, newVal) -> {
			searchBy.getSelectedToggle();
			if (searchBy.getSelectedToggle() == searchByDate) {
				fromDate.setDisable(false);
				toDate.setDisable(false);
				choiceBox1.setDisable(true);
				choiceBox2.setDisable(true);
				firstTag.setDisable(true);
				secondTag.setDisable(true);
				searchByOneTag.setDisable(true);
				searchByTwoTags.setDisable(true);
				andOperator.setDisable(true);
				orOperator.setDisable(true);
			}
			
			if (searchBy.getSelectedToggle() == searchByTags) {
				fromDate.setDisable(true);
				toDate.setDisable(true);
				choiceBox1.setDisable(false);
				choiceBox2.setDisable(false);
				firstTag.setDisable(false);
				secondTag.setDisable(false);
				searchByOneTag.setDisable(false);
				searchByTwoTags.setDisable(false);
				andOperator.setDisable(false);
				orOperator.setDisable(false);
			}
		});
		
		ToggleGroup tags = new ToggleGroup();
		searchByOneTag.setToggleGroup(tags);
		searchByTwoTags.setToggleGroup(tags);
		tags.selectedToggleProperty().addListener((observable, oldVal, newVal) -> {
			searchBy.getSelectedToggle();
		
			if (tags.getSelectedToggle() == searchByOneTag) {
				choiceBox1.setDisable(false);
				firstTag.setDisable(false);
				choiceBox2.setDisable(true);
				secondTag.setDisable(true);
				andOperator.setSelected(false);
				orOperator.setSelected(false);
				andOperator.setDisable(true);
				orOperator.setDisable(true);
			}
			
			else if (tags.getSelectedToggle() == searchByTwoTags) {
				choiceBox1.setDisable(false);
				firstTag.setDisable(false);
				choiceBox2.setDisable(false);
				secondTag.setDisable(false);
				andOperator.setDisable(false);
				orOperator.setDisable(false);
			}
		}); 
		
		ToggleGroup operators = new ToggleGroup();
		andOperator.setToggleGroup(operators);
		orOperator.setToggleGroup(operators);
		operators.selectedToggleProperty().addListener((observable, oldVal, newVal) -> searchBy.getSelectedToggle());
		
		paths = FXCollections.observableArrayList();
		captions = FXCollections.observableArrayList();
		listview.setItems(paths);
		
		listview.setCellFactory(listView -> new ListCell<String>() {
			private ImageView imageView;
			private String caption;
			@Override
			public void updateItem(String text, boolean empty) {
				super.updateItem(text, empty);
				if (empty) {
					setText(null);
					setGraphic(null);
				} else {
					caption = captions.get(paths.indexOf(text));
					imageView = getImageView(text);
					if (imageView == null) {
						setText(null);
						setGraphic(null);
					} else {
						setText(caption);
						setGraphic(imageView);
					}
				}
			}
			
		});
		
	}
	
	/**
	 * Updates listview to display search results. Accepts an album input.
	 * 
	 * @param searchResults
	 */
	public void displaySearchResults(Album searchResults) {
		paths.clear();
		captions.clear();
		for (Photo photo : searchResults.getPhotos()) {
			captions.add(photo.getCaption());
			paths.add(photo.getPath());
		}
	}
	
	/**
	 * Returns an ImageView given an image path String
	 * 
	 * @param path
	 * @return ImageView
	 */
	public ImageView getImageView(String path) {
		final File f = new File(path);
		try {
			InputStream is = new FileInputStream(f);
			final Image image = new Image(is);
			ImageView iv = new ImageView(image);
			iv.setFitHeight(50);
			iv.setFitWidth(75);
			return iv;
		} catch (FileNotFoundException e) {
			return null;
		}
	}
	
	/**
	 * Called when 'Search' is clicked. Search's the users photos in each album based on the specified tag.
	 */
	public void searchPhotos() {
		User currentUser = terminal.getActiveUser();
		
		//check if search is by date or by tags
		if (!searchByDate.isSelected() && !searchByTags.isSelected()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Unavailable search.");
			alert.setContentText("Please search either by date or by tags.");
			searchByDate.setSelected(false);
			searchByTags.setSelected(false);
			alert.showAndWait();
		}
		
		//search by date
		else if (searchByDate.isSelected()) {
			if (fromDate.getValue() != null && toDate.getValue() != null) {
				LocalDate from = fromDate.getValue();
				LocalDate to = toDate.getValue();
				if (to.isBefore(from)) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Dates.");
					alert.setContentText("Please make sure that the 'From Date' is before 'To Date'.");
					alert.showAndWait();
				} else {
					searchResultsAlbum = new Album("searchResultsAlbums");
						ArrayList<Photo> allphotos = currentUser.getAllPhotos();
						for (Photo photo : allphotos) {
							Date photoDate = photo.getDateofPhoto();
							LocalDate date = photo.getLocalDate(photoDate);
							if ((date.isAfter(from) || date.isEqual(from)) && (date.isBefore(to) || date.isEqual(to))) {
								searchResultsAlbum.addStockPhoto(photo);
							}
							else {
								continue;
							}
						}
					
					displaySearchResults(searchResultsAlbum);
				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Invalid Dates.");
				alert.setContentText("Please input dates to search.");
				fromDate.setValue(null);
				toDate.setValue(null);
				alert.showAndWait();
			}
		} 
		
		//search by tags
		else {
			//one tag search
			if (!searchByOneTag.isSelected() && !searchByTwoTags.isSelected()) { 
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Invalid Search Criteria.");
				alert.setContentText("Please search by one tag or by two tags.");
				alert.showAndWait();
			}
			
			else if (searchByOneTag.isSelected()) {
				if (choiceBox1.getSelectionModel().getSelectedItem() == null) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Tag Selection.");
					alert.setContentText("Please select a tag to search by.");
					alert.showAndWait();
				}
				
				else if (firstTag.getText().isBlank()) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Tag Input.");
					alert.setContentText("Please input a valid tag.");
					alert.showAndWait();
				}
				
				else { 
					searchResultsAlbum = new Album("searchResultsAlbums");

						ArrayList<Photo> photosInAlbum = currentUser.getAllPhotos();
						for (Photo photo : photosInAlbum) {
							ArrayList<String> photoTags = photo.getTags();
							String tagSearch = "[" + choiceBox1.getValue() + ": " + firstTag.getText() + "]";
							if (photoTags.contains(tagSearch)) {
								searchResultsAlbum.addStockPhoto(photo);
							} else {
								continue;
							}
						}
					
					displaySearchResults(searchResultsAlbum);
				}
			}
			//two tag search
			else {
				if (!andOperator.isSelected() && !orOperator.isSelected()) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Search Criteria.");
					alert.setContentText("Please select either 'And' or 'Or'.");
					alert.showAndWait();
				}
				else if (choiceBox1.getSelectionModel().getSelectedItem() == null
						|| choiceBox2.getSelectionModel().getSelectedItem() == null) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Tag Selection.");
					alert.setContentText("Please select both tags to search by.");
					alert.showAndWait();
				}
				else if (firstTag.getText().isBlank() || secondTag.getText().isBlank()) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Invalid Tag Input.");
					alert.setContentText("Please input two valid tags.");
					alert.showAndWait();
				}
				else { 
					searchResultsAlbum = new Album("searchResultsAlbums");
					if (andOperator.isSelected()) {
							ArrayList<Photo> photosInAlbum = currentUser.getAllPhotos();
							for (Photo photo : photosInAlbum) {
								ArrayList<String> photoTags = photo.getTags();
								String firstTagSearch = "[" + choiceBox1.getValue() + ": " + firstTag.getText() + "]";
								String secondTagSearch = "[" + choiceBox2.getValue() + ": " + secondTag.getText() + "]";
								if (photoTags.contains(firstTagSearch) && photoTags.contains(secondTagSearch)) {
									searchResultsAlbum.addStockPhoto(photo);
								} else {
									continue;
								}
							}
						
						displaySearchResults(searchResultsAlbum);
					}
					
					else {
							ArrayList<Photo> photosInAlbum = currentUser.getAllPhotos();
							for (Photo photo : photosInAlbum) {
								ArrayList<String> photoTags = photo.getTags();
								String firstTagSearch = "[" + choiceBox1.getValue() + ": " + firstTag.getText() + "]";
								String secondTagSearch = "[" + choiceBox2.getValue() + ": " + secondTag.getText() + "]";
								if (photoTags.contains(firstTagSearch) || photoTags.contains(secondTagSearch)) {
									searchResultsAlbum.addStockPhoto(photo);
								} else {
									continue;
								}
							}
						
						displaySearchResults(searchResultsAlbum);
					}
				}
			}
		}
	}
	
	/**
	 * Called when 'Create Album From Results' is clicked. Adds new album, based on most recent search results, to the current user's list of albums.
	 */
	public void createAlbumFromSearch() {
		int check = listview.getItems().size();
		if (check == 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Unable to create album.");
			alert.setContentText("Please search for photos first.");
			alert.showAndWait();
		} else {
		User currentUser = terminal.getActiveUser();
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Create Album from Search Results");
		dialog.setHeaderText("Creating New Album");
		dialog.setContentText("Please enter a valid album name:");
		Optional<String> result = dialog.showAndWait();
		
		if (result.isPresent()) {
		    String albumName = result.get().replaceAll("\\s","");
		    if (albumName.trim().length() > 0) {
			    if (currentUser.hasAlbum(albumName)) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Adding new album has been unsuccessful.");
					alert.setContentText("This album already exists! Choose a different album name.");
					alert.showAndWait();
			    } else {
					if (currentUser.addAlbum(albumName)) {
						Album searchAlbum = currentUser.getAlbum(albumName);
						for (Photo photo : searchResultsAlbum.getPhotos()) {
							searchAlbum.addStockPhoto(photo);
						}
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Confirmation");
						alert.setHeaderText("Creating new album has been successful.");
						alert.setContentText("Album successfully created!");
						alert.showAndWait();
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error Dialog");
						alert.setHeaderText("Adding new album has been unsuccessful.");
						alert.setContentText("This album already exists! Choose a different album name.");
						alert.showAndWait();
					}
			    }
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Adding new album has been unsuccessful.");
				alert.setContentText("Choose a valid album name.");
				alert.showAndWait();
			} 
		} else {
//			Alert alert = new Alert(AlertType.ERROR);
//			alert.setTitle("Error Dialog");
//			alert.setHeaderText("Adding new album has been unsuccessful.");
//			alert.setContentText("Type in a valid album name.");
//			alert.showAndWait();
		}
		}
	}
	
	/**
	 * Called when 'back' is clicked. Returns to Albums page.
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/albums.fxml"));
		Parent pane = loader.load(); 
		
		AlbumsController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	/**
	 * Called when 'logout' is clicked. Returns to login page.
	 * @throws java.io.IOException
	 */
	public void logoutAttempt() throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent pane = loader.load(); 
		
		LoginController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
}