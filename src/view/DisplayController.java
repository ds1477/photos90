package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Terminal;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The DisplayController class is the controller for display.fxml. Displays a photo in slideshow form, and displays information of photo.
 */
public class DisplayController {
	
	/**
	 * Button to return to Photos page
	 */
	@FXML
	public Button back;
	
	/**
	 * Button to go to Display page of next photo in the album
	 */
	@FXML
	public Button next;
	
	/**
	 * Button to go to the Display page of the previous photo in the album
	 */
	@FXML
	public Button previous;
	
	/**
	 * ImageView of photo to display
	 */
	@FXML
	public ImageView imageview;
	
	/**
	 * TextArea for the date of the photo
	 */
	@FXML
	public TextArea date;
	
	/**
	 * TextArea for the caption of the photo
	 */
	@FXML
	public TextArea caption;
	
	/**
	 * TextArea for the tags of the photo
	 */
	@FXML
	public TextArea tags;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * The activeAlbum of the photo being displayed
	 */
	Album currentAlbum;
	
	/**
	 * The activePhoto, the photo being displayed
	 */
	Photo currentPhoto;
	
	/**
	 * All the photos in the activeAlbum
	 */
	ArrayList<Photo> albumPhotos;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		currentPhoto = terminal.getActivePhoto();
		currentAlbum = terminal.getActiveAlbum();
		albumPhotos = currentAlbum.getPhotos();
		date.setText(currentPhoto.getDate());
		date.setEditable(false);
		caption.setText(currentPhoto.getCaption());
		caption.setEditable(false);
		String alltags = "";
		ArrayList<String> phototags = currentPhoto.getTags();
		if (phototags.size() > 0) {
			alltags = phototags.get(0);
			for (int i = 1; i < phototags.size(); i++) {
				alltags = alltags + ", " + phototags.get(i);
			}
		}
		tags.setText(alltags);
		tags.setEditable(false);
		File file = new File(currentPhoto.getPath());
		try {
			InputStream is = new FileInputStream(file);
			final Image image = new Image(is);
			imageview.setImage(image);
		} catch (FileNotFoundException e) {
			System.out.println("Unexpected photo loading error");
		}
		
	}
	
	/**
	 * Called when 'next' is clicked. Attempts to go to the display page of the next photo in the album
	 */
	public void nextPhoto() {
		int index = albumPhotos.indexOf(currentPhoto);
		if (index == albumPhotos.size() - 1) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Alert");
			alert.setHeaderText("Next photo does not exist.");
			alert.setContentText("Reached end of slideshow.");
			alert.showAndWait();
		} else {
			Photo to = albumPhotos.get(index + 1);
			terminal.setActivePhoto(to.getPath());
			reload();
		}
	}
	
	/**
	 * Called when 'previous' is clicked. Attempts to go to the display page of the previous photo in the album
	 */
	public void prevPhoto() {
		int index = albumPhotos.indexOf(currentPhoto);
		if (index == 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Alert");
			alert.setHeaderText("Previous photo does not exist.");
			alert.setContentText("This is the first photo of the slideshow.");
			alert.showAndWait();
		} else {
			Photo to = albumPhotos.get(index - 1);
			terminal.setActivePhoto(to.getPath());
			reload();
		}
	}
	
	/**
	 * Method that reloads the Display page, when changing the photo being displayed
	 */
	public void reload() {
		try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/display.fxml"));
		Parent pane = loader.load(); 
		
		DisplayController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
		} catch (IOException e) {
			System.out.println("Unexpected Error");
		}
	}
	
	/**
	 * Called when 'back' is clicked. Returns to photos page.
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/photos.fxml"));
		Parent pane = loader.load(); 
		
		PhotosController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	
}