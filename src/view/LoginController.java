package view;

import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The LoginController class is the controller for login.fxml.
 */
public class LoginController {
	
	/**
	 * TextField that receives username
	 */
	@FXML
	public TextField user;
	
	/**
	 * Button to log in
	 */
	@FXML
	public Button login;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * The terminal instance
	 */
	Terminal terminal;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.primaryStage = primaryStage;
		this.terminal = terminal;
		
	}
	
	/**
	 * Called when 'login' is clicked. Will either go to admin page if "admin" is entered into the 'user' TextField, or the Albums page otherwise.
	 * @throws java.io.IOException
	 */
	public void loginAttempt() throws IOException {
		
		if (user.getText().equals(null)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Login unsuccessful.");
			alert.setContentText("Please enter a valid username!");
			alert.showAndWait();
		} else if (user.getText().equals("")) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Login unsuccessful.");
			alert.setContentText("Please enter a valid username!");
			alert.showAndWait();
		} else if (user.getText().equals("admin")) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/admin.fxml"));
			Parent pane = loader.load(); 
			
			AdminController controller = loader.getController(); 
			controller.start(this.primaryStage, this.terminal); 
			primaryStage.getScene().setRoot(pane);
			primaryStage.sizeToScene();
		} else if (user.getText().equals("stock")) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/albums.fxml"));
			Parent pane = loader.load(); 
			terminal.setActiveUser("stock");
			
			AlbumsController controller = loader.getController(); 
			controller.start(this.primaryStage, this.terminal); 
			primaryStage.getScene().setRoot(pane);
			primaryStage.sizeToScene();
		} else {
			ArrayList<String> validUsers = terminal.getUsers();
			String username = user.getText();
			if (validUsers.contains(username)) {
				terminal.setActiveUser(username);
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/albums.fxml"));
				Parent pane = loader.load(); 
				
				AlbumsController controller = loader.getController(); 
				controller.start(this.primaryStage, this.terminal); 
				primaryStage.getScene().setRoot(pane);
				primaryStage.sizeToScene();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Login unsuccessful.");
				alert.setContentText("This username does not exist!");
				alert.showAndWait();
			}
			
		}
		

	
	}
	
}