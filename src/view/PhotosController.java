package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Terminal;
import model.User;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * PhotosController is the controller for photos.fxml. Displays photo thumbnails and allows access to the Display, Captions, Tags, and Move/Copy pages.
 */
public class PhotosController {
	
	/**
	 * The button that goes to the tags page
	 */
	@FXML
	public Button tag;
	
	/**
	 * The logout button
	 */
	@FXML
	public Button logout;
	
	/**
	 * The back button
	 */
	@FXML
	public Button back;
	
	/**
	 * The button that goes to the Move/Copy page
	 */
	@FXML
	public Button move;
	
	/**
	 * The button that goes to the edit captions page
	 */
	@FXML
	public Button caption;
	
	/**
	 * The button that goes to the Display page, for the first photo in the album
	 */
	@FXML
	public Button displayF;
	
	/**
	 * The button that goes to the Display page, for the selected photo in the list
	 */
	@FXML
	public Button displayS;
	
	/**
	 * The listview of photo thumbnails and captions
	 */
	@FXML
	public ListView<String> listview;
	
	/**
	 * The button to add a photo
	 */
	@FXML
	public Button add;
	
	/**
	 * The button to delete a photo
	 */
	@FXML
	public Button delete;
	
	/**
	 * The field that accepts a photo path
	 */
	@FXML
	public TextField newPhoto;
	
	/**
	 * The title of the page. Is changed depending on what album is opened.
	 */
	@FXML
	public TextArea title;
	
	/**
	 * The primary stage
	 */
	Stage primaryStage;
	
	/**
	 * Terminal instance
	 */
	Terminal terminal;
	
	/**
	 * List of captions that will be used in populating the listview
	 */
	ObservableList<String> captions;
	
	/**
	 * Start method, is called first. Initial set up for the page
	 * 
	 * @param primaryStage
	 * @param terminal
	 */
	public void start(Stage primaryStage, Terminal terminal) {
		this.terminal = terminal;
		this.primaryStage = primaryStage;
		
		title.setText("Photos in " + Album.getAlbumName(terminal.getActiveAlbum()));
		title.setEditable(false);
		
		Album active = terminal.getActiveAlbum();
				
		captions = FXCollections.observableArrayList();
		ObservableList<String> paths = FXCollections.observableArrayList();
		//ArrayList<String> paths = new ArrayList<String>();
		
		for (Photo photo : active.getPhotos()) {
			captions.add(photo.getCaption());
			paths.add(photo.getPath());
		}
		
		listview.setItems(paths);
		
		listview.setCellFactory(listView -> new ListCell<String>() {
			private ImageView imageView;
			private String caption;
			@Override
			public void updateItem(String text, boolean empty) {
				super.updateItem(text, empty);
				if (empty) {
					setText(null);
					setGraphic(null);
				} else {
					caption = captions.get(paths.indexOf(text));
					imageView = getImageView(text);
					if (imageView == null) {
						setText(null);
						setGraphic(null);
					} else {
						setText(caption);
						setGraphic(imageView);
					}
				}
			}
			
		});
		
	}
	
	/**
	 * Returns an ImageView given an image path String
	 * 
	 * @param path
	 * @return ImageView
	 */
	public ImageView getImageView(String path) {
		final File f = new File(path);
		try {
			InputStream is = new FileInputStream(f);
			final Image image = new Image(is);
			ImageView iv = new ImageView(image);
			iv.setFitHeight(50);
			iv.setFitWidth(75);
			return iv;
		} catch (FileNotFoundException e) {
			return null;
		}
	}
	
	/**
	 * Called when the 'add' button is clicked. Attempts to add a photo to the album.
	 */
	public void addPhoto() {
		String path = newPhoto.getText();
		if (path == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Adding photo has been unsuccessful.");
			alert.setContentText("Please enter a photo path.");
			alert.showAndWait();
		} else {
				File file = new File(path);
				if (file.exists()) {
					Photo photo = new Photo(path);
					User currentUser = terminal.getActiveUser();
					Album currentAlbum = terminal.getActiveAlbum();
					if (currentUser.addPhoto(photo, currentAlbum)) {
						captions.add(null);
						listview.getItems().add(path);
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error Dialog");
						alert.setHeaderText("Adding photo has been unsuccessful.");
						alert.setContentText("This photo is already in the album.");
						alert.showAndWait();
					}
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Adding photo has been unsuccessful.");
					alert.setContentText("This photo does not exist. Please enter a valid path. Example: C:/users/username/Downloads/example.png");
					alert.showAndWait();
				}
		}
	}
	
	/**
	 * Called when the 'delete' button is clicked. Attempts to delete the selected photo from the album.
	 */
	public void deletePhoto() {
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Deleting photo has been unsuccessful.");
			alert.setContentText("Please select a photo to delete.");
			alert.showAndWait();
		}
		else {
			Album currentAlbum = terminal.getActiveAlbum();
			int index = listview.getSelectionModel().getSelectedIndex();
			String path = listview.getSelectionModel().getSelectedItem();
			Photo deleteme = currentAlbum.getPhoto(path);
			if (currentAlbum.deletePhoto(deleteme)) {
				listview.getItems().remove(index);
				captions.remove(index);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText("Deleting photo has been unsuccessful.");
				alert.setContentText("Please try again.");
				alert.showAndWait();
			}
		}
	}
	
	/**
	 * Called when the 'back' button is clicked. Returns to the Albums page.
	 * @throws java.io.IOException
	 */
	public void goBack() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/albums.fxml"));
		Parent pane = loader.load();
		terminal.closeActiveAlbum();
		
		AlbumsController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	}
	
	/**
	 * Called with 'displayF' is clicked. Attempts to display the first photo.
	 * @throws java.io.IOException
	 */
	public void toDisplay1() throws IOException { //from first item
		int items = listview.getItems().size();
		if (items == 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Displaying photo has been unsuccessful.");
			alert.setContentText("No photos to display!");
			alert.showAndWait();
		} else {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/display.fxml"));
			Parent pane = loader.load(); 
			terminal.setActivePhoto(listview.getItems().get(0));
			
			DisplayController controller = loader.getController(); 
			controller.start(this.primaryStage, this.terminal); 
			primaryStage.getScene().setRoot(pane);
			primaryStage.sizeToScene();
		}
		
	}
	
	/**
	 * Called when 'displayS' is clicked. Attempts to display the selected photo.
	 * @throws java.io.IOException
	 */
	public void toDisplay2() throws IOException { //from selected item
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Displaying photo has been unsuccessful.");
			alert.setContentText("Please select a photo first!");
			alert.showAndWait();
		} else {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/display.fxml"));
		Parent pane = loader.load(); 
		terminal.setActivePhoto(listview.getSelectionModel().getSelectedItem());
		
		DisplayController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
		}
	}
	
	/**
	 * Called when the 'caption' button is clicked. Attempts to go to the edit caption page for selected photo.
	 * @throws java.io.IOException
	 */
	public void toCaption() throws IOException {
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Viewing caption of photo has been unsuccessful.");
			alert.setContentText("Please select a photo first!");
			alert.showAndWait();
		} else {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/caption.fxml"));
		Parent pane = loader.load(); 
		terminal.setActivePhoto(listview.getSelectionModel().getSelectedItem());
		
		CaptionController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
		}
	}
	
	/**
	 * Called when the 'move' button is clicked. Attempts to go to the Move/Copy page for selected photo.
	 * @throws java.io.IOException
	 */
	public void toMove() throws IOException {
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Viewing move/copy page of photo has been unsuccessful.");
			alert.setContentText("Please select a photo first!");
			alert.showAndWait();
		} else {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/move_copy.fxml"));
		Parent pane = loader.load(); 
		terminal.setActivePhoto(listview.getSelectionModel().getSelectedItem());
		
		MoveCopyController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
		}
	}
	
	/**
	 * Called when 'tag' button is clicked. Attempts to go to the edit tags page of the selected photo. 
	 * @throws java.io.IOException
	 */
	public void toTags() throws IOException {
		if (listview.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Viewing tags of photo has been unsuccessful.");
			alert.setContentText("Please select a photo first!");
			alert.showAndWait();
		} else {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/tags.fxml"));
		Parent pane = loader.load(); 
		terminal.setActivePhoto(listview.getSelectionModel().getSelectedItem());
		
		TagsController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
		}
	}
	
	/**
	 * Called when 'logout' button is clicked. Attempts to return to the login page.
	 * @throws java.io.IOException
	 */
	public void logoutAttempt() throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent pane = loader.load(); 
		terminal.clearActives();
		
		LoginController controller = loader.getController(); 
		controller.start(this.primaryStage, this.terminal); 
		primaryStage.getScene().setRoot(pane);
		primaryStage.sizeToScene();
	

	
	}
	
}