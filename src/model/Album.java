package model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The Album class stores information for an Album, and has methods for editing and retrieving information about the photos inside the album. 
 */

public class Album implements Serializable{
	
	/**
	 * Serial Version ID is set to 1
	 */
	public static final long serialVersionUID = 1L;
	
	/**
	 * Name of the album. Uniquely identifies an album.
	 */
	protected String name;
	
	/**
	 * Stores all the photos in a particular album.
	 */
	protected ArrayList<Photo> photos;
	
	/**
	 * Album constructor, accepts one String argument.
	 * 
	 * @param name
	 */
	public Album(String name) {
		this.name = name;
		photos = new ArrayList<Photo>();
	}
	
	public boolean hasPhoto(String path) {
		for (Photo photo: photos) {
			if (photo.imagePath.equals(path)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Deletes a photo from the album. Returns true if success, false if photo is not in album.
	 * 
	 * @param photo
	 */
	public boolean deletePhoto(Photo photo) {
		if (photos.contains(photo)) {
			int index = photos.indexOf(photo);
			photos.remove(index);
			return true;
		}
		return false;
	}
	
	/**
	 * Adds a photo to the album directly. Particularly used for adding stock photos to the stock account's Stock album.
	 * Only use when photo is safely add-able.
	 * 
	 * @param photo
	 */
	public void addStockPhoto(Photo photo) {
		photos.add(photo);
	}
	
	/**
	 * Returns a photo object given a particular path String.
	 * 
	 * @param path
	 * @return photo
	 */
	public Photo getPhoto(String path) {
		for (Photo photo : photos) {
			if (photo.imagePath.equals(path)) {
				return photo;
			}
		}
		return null;
	}
	
	/**
	 * Returns all photos in the album
	 * 
	 * @return ArrayList of photos
	 */
	public ArrayList<Photo> getPhotos() {
		return photos;
	}
	
	/**
	 * Returns the name of an album.
	 * 
	 * @param album
	 * @return album name
	 */
	public static String getAlbumName(Album album) {
		return album.name;
	}
	
	/**
	 * Returns the number of photos in the album.
	 * 
	 * @return int
	 */
	public int getNumberOfPhotos() {
		return photos.size();
	}
	
	/**
	 * Returns an ArrayList of dates of size 2. The first element of the ArrayList is the earliest photo date of all photos in the album.
	 * The second is the latest photo date of the album.
	 * 
	 * @return ArrayList of dates
	 */
	public ArrayList<Date> getTimeRange() {
		Calendar earlycal = Calendar.getInstance();
		Calendar latecal = Calendar.getInstance();
		earlycal.set(Calendar.MILLISECOND, 0);
		latecal.set(Calendar.MILLISECOND, 0);
		
		ArrayList<Date> range = new ArrayList<Date>();
		range.add(null);
		range.add(null);
		if (photos.isEmpty()) {
			return range;
		}
		earlycal.setTimeInMillis(new File(photos.get(0).imagePath).lastModified());
		latecal.setTimeInMillis(new File(photos.get(0).imagePath).lastModified());
		range.set(0, earlycal.getTime());
		range.set(1, latecal.getTime());
		for (Photo photo : photos) {
			if (photo.date.compareTo(range.get(0)) < 0) {
				range.set(0, photo.date);
			} else if (photo.date.compareTo(range.get(1)) > 0) {
				range.set(1, photo.date);
			}
		}
		return range;
	}
	
	
}