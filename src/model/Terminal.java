package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.channels.NonWritableChannelException;
import java.util.ArrayList;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The Terminal class stores information about all users, and is used to keep track of what the system is doing. 
 */

public class Terminal implements Serializable {

	/**
	 * The location of the dat file is in the src folder
	 */
	public static final String storeDir = "src";
	
	/**
	 * Data is stored in userInfo.dat
	 */
	public static final String storeFile = "userInfo.dat";
	
	/**
	 * Serial Version ID is set to 1
	 */
	public static final long serialVersionUID = 1L;
	
	/**
	 * List of all users
	 */
	protected ArrayList<User> users;
	
	/**
	 * The user that has logged in, if applicable (is null otherwise)
	 */
	protected User activeUser;
	
	/**
	 * The album that has been opened, if applicable (is null otherwise)
	 */
	protected Album activeAlbum;
	
	/**
	 * The photo that is being acted upon, if applicable (is null otherwise)
	 */
	protected Photo activePhoto;
	
	
	/**
	 * No argument constructor for a Terminal
	 */
	public Terminal() {
		users = new ArrayList<User>();
		activeUser = null;
		activeAlbum = null;
		activePhoto = null;
	}
	
	/**
	 * One argument constructor for a Terminal. Accepts another terminal as a parameter. Used for serialization.
	 * 
	 * @param terminal
	 */
	public Terminal(Terminal term) { //This is because the onClose thing on Photos.java only accepts a final Terminal
		users = term.users;
		activeUser = null; //If you close the app it's just going to log you out anyway
		activeAlbum = null;
		activePhoto = null;
	}
	
	/**
	 * Deserializes the data from storage. Returns a terminal.
	 * 
	 * @return terminal
	 */
	public static Terminal readApp() throws ClassNotFoundException {
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new FileInputStream(storeDir + File.separator + storeFile));
			Terminal term = (Terminal)ois.readObject();
			ois.close();
			return term;
		} catch (IOException e) {
			return null;
		}
	} //This is for deserializing objects from storage
	
	/**
	 * Serializes a terminal, called with closing the application.
	 * 
	 * @param terminal
	 */
	public static void writeApp(Terminal term) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeDir + File.separator + storeFile));
		oos.writeObject(term);
		oos.close();
	} //This is for serializing objects (putting into storage)
	
	
	/**
	 * Sets the activeUser, given a username String.
	 * 
	 * @param username
	 */
	public void setActiveUser(String username) {
		for (User user : users) {
			if (user.username.equals(username)) {
				this.activeUser = user;
			}
		}
	}
	
	/**
	 * Used when logging into the stock user account. Accepts the stock user as an argument.
	 * 
	 * @param user
	 */
	public void setStockUser(User stock) {
		this.activeUser = stock;
	}
	
	/**
	 * Sets the activeAlbum, given an album name String
	 * 
	 * @param albumname
	 */
	public void setActiveAlbum(String albumname) {
		for (Album album : activeUser.getAllAlbums()) {
			if (Album.getAlbumName(album).equals(albumname)) {
				this.activeAlbum = album;
			}
		}
	}
	
	/**
	 * Sets the activePhoto given a path String.
	 * 
	 * @param path
	 */
	public void setActivePhoto(String path) {
		for (Photo photo : activeAlbum.getPhotos()) {
			if (photo.imagePath.equals(path)) {
				this.activePhoto = photo;
			}
		}
	}
	
	/**
	 * Sets the activeUser to null
	 */
	public void closeActiveUser() {
		this.activeUser = null;
	}
	
	/**
	 * Sets the activeAlbum to null
	 */
	public void closeActiveAlbum() {
		this.activeAlbum = null;
	}
	
	/**
	 * Sets the activePhoto to null
	 */
	public void closeActivePhoto() {
		this.activePhoto = null;
	}
	
	/**
	 * Sets activePhoto, activeAlbum, and activeUser all to null. Particularly used with logging out.
	 */
	public void clearActives() {
		this.activeUser = null;
		this.activePhoto = null;
		this.activeAlbum = null;
	}
	
	/**
	 * Returns the activeUser
	 * 
	 * @return activeUser
	 */
	public User getActiveUser() {
		return this.activeUser;
	}
	
	/**
	 * Returns the activeAlbum
	 * 
	 * @return activeAlbum
	 */
	public Album getActiveAlbum() {
		return this.activeAlbum;
	}
	
	/**
	 * Returns the activePhoto
	 * 
	 * @return activePhoto
	 */
	public Photo getActivePhoto() {
		return this.activePhoto;
	}
	
	/**
	 * Returns all the usernames as an ArrayList of username Strings.
	 * 
	 * @return ArrayList of strings
	 */
	public ArrayList<String> getUsers() { //Returns all usernames
		ArrayList<String> usernames = new ArrayList<String>();
		for (User user : users) {
			usernames.add(user.username);
		}
		return usernames;
	}
	
	/**
	 * Adds a user by a username String. Returns true if success, throws an IOException if the user is already in the list.
	 * 
	 * @param name
	 * @return boolean
	 */
	public boolean addUser(String name) {
		if (users.isEmpty()) { //this shouldn't be possible tbh but just in case
			User user = new User(name);
			users.add(user);
			return true;
		} else {
			ArrayList<String> usernames = getUsers();
			if (usernames.contains(name)) {
				return false;
			} else {
				User user = new User(name);
				users.add(user);
				return true;
			}
		}
	}
	
	/**
	 * Deletes a user, given a username String. Returns true if success, false if user is not in list.
	 * 
	 * @param name
	 * @return boolean
	 */
	public boolean deleteUser(String name) {
		User u = null;
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			String username = User.getUsername(user);
			if (username.equals(name)) {
				u = user;
				int index = users.indexOf(u);
				users.remove(index);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns a user given a username String
	 * 
	 * @param name
	 * @return user
	 */
	public User getUser(String name) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			String username = User.getUsername(user);
			if (username.equals(name)) {
				return user;
			}
		}
		return null;
	}
	
}