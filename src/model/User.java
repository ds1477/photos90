package model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The User class stores information for a User, and has methods for editing and retrieving information about albums. 
 */

public class User implements Serializable{
	
	/**
	 * Serial version ID is set to 1
	 */
	public static final long serialVersionUID = 1L;
	
	/**
	 * String username, used to uniquely identify a user
	 */
	protected String username;
	
	/**
	 * Stores all the albums that the User has created
	 */
	protected ArrayList<Album> albums;
	
	/**
	 * Stores all the tag types, including preloaded tag types and user created
	 */
	protected ArrayList<String> tagTypes;
	
	
	/**
	 * Constructor for a User. Accepts one String argument.
	 * 
	 * @param name 
	 */
	public User(String name) {
		username = name;
		albums = new ArrayList<Album>();
		tagTypes = new ArrayList<String>();
		tagTypes.add("Person");
		tagTypes.add("Location");
	}
	
	/**
	 * Used for returning the username of a User object
	 * 
	 * @param user
	 * @return username
	 */
	public static String getUsername(User user) {
		return user.username;
	}
	
	/**
	 * Used for returning the tag types of a user.
	 * 
	 * @return ArrayList of strings, tagTypes
	 */
	public ArrayList<String> getTagTypes() {
		return tagTypes;
	}
	
	/**
	 * Adds a tag to the list of tag types. Returns true if success, false if tag type already is in list.
	 * 
	 * @param tag
	 * @return boolean
	 */
	public boolean addTagType(String tag) {
		if (tagTypes.contains(tag)) {
			return false;
		} else {
			tagTypes.add(tag);
			return true;
		}
	}
	
	/**
	 * Used to see if a user has already created an album. Returns true if the album exists, false if not.
	 * 
	 * @param name
	 * @return boolean
	 */
	public boolean hasAlbum(String name) {
		for (String albumname : this.getAlbumNames()) {
			if (albumname.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Adds an album to a user's list of albums. Returns true if success, false if album already exists.
	 * 
	 * @param name
	 * @return boolean
	 */
	public boolean addAlbum(String name) {
		if (albums.isEmpty()) {
			Album newAlbum = new Album(name);
			albums.add(newAlbum); 
			return true;
		} else if (!albums.isEmpty()) {
			ArrayList<String> names = this.getAlbumNames();
			if (!names.contains(name)) {
				Album newAlbum = new Album(name);
				albums.add(newAlbum); 
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Used for updating the name of an album. Returns true if success, false if name to be changed is not an existent album.
	 * 
	 * @param oldName
	 * @param newName
	 * @return boolean
	 */
	public boolean changeAlbum(String oldName, String newName) {
		for (int i = 0; i < albums.size(); i++) {
			Album a = albums.get(i);
			String albumName = Album.getAlbumName(a);
			if (albumName.equals(oldName) && !albums.contains(getAlbum(newName))) {
				a.name = newName;
				return true;
			} 
		}	
		return false;
	}
	
	/**
	 * Deletes an album. Returns true if success, false if album not found.
	 * 
	 * @param name
	 * @return boolean
	 */
	public boolean deleteAlbum(String name) {
		Album a = null;
		for (int i = 0; i < albums.size(); i++) {
			Album album = albums.get(i);
			String albumName = Album.getAlbumName(album);
			if (albumName.equals(name)) {
				a = album;
				int index = albums.indexOf(a);
				albums.remove(index);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Adds a photo to an album. Returns true if success, false if image is already in album.
	 * 
	 * @param photo
	 * @param album
	 * @return boolean
	 */
	public boolean addPhoto(Photo photo, Album album) {
		for (Photo p: album.photos) {
			if (p.imagePath.equals(photo.imagePath)) {
				return false;
			}
		}
		ArrayList<Photo> allPhotos = this.getAllPhotos();
		Photo toAdd = photo;
		for (Photo p : allPhotos) {
			if (p.imagePath.equals(photo.imagePath)) {
				toAdd = p;
			}
		}
		album.photos.add(toAdd);
		return true;
	}
	
	/**
	 * Gets an album, given an album name.
	 * 
	 * @param name
	 * @return album
	 */
	public Album getAlbum(String name) {
		for (int i = 0; i < albums.size(); i++) {
			Album album = albums.get(i);
			String albumName = Album.getAlbumName(album);
			if (albumName.equals(name)) {
				return album;
			}
		}
		return null;
	}
	
	/**
	 * Returns the list of all albums for a user.
	 * 
	 * @return ArrayList of albums
	 */
	public ArrayList<Album> getAllAlbums() {
		return albums;
	}
	
	/**
	 * Returns the list of all photos in all albums for a user.
	 * 
	 * @return ArrayList of photos
	 */
	public ArrayList<Photo> getAllPhotos() {
		ArrayList<Photo> allphotos = new ArrayList<Photo>();
		for (Album album : albums) {
			for (Photo photo : album.getPhotos()) {
				if (!allphotos.contains(photo)) {
					allphotos.add(photo);
				}
			}
		}
		return allphotos;
	}
	
	/**
	 * Returns a list of all album names for a user.
	 * 
	 * @return ArrayList of Strings
	 */
	public ArrayList<String> getAlbumNames() {
		ArrayList<String> albumnames = new ArrayList<String>();
		for (Album album : albums) {
			albumnames.add(album.name);
		}
		return albumnames;
	}
	
}